%define modname etercifs

Name: dkms-etercifs
Version: 4.5.0
Release: alt1
Summary: DKMS-ready CIFS Linux kernel module with Etersoft extensions
License: GPL
Packager: Yury Fil <yurifil@altlinux.org>

# Note: this path parsed by etersoft-build-utils for target SRPM dir detecting
#Source: ftp://updates.etersoft.ru/pub/Etersoft/CIFS@Etersoft/%version/sources/tarball/%name-%version.tar.bz2

Group: Development/Kernel
Requires(preun): dkms
Requires(post): dkms

Requires: etercifs = %version

Buildarch: noarch

%description
The CIFS VFS is a virtual file system for Linux to allow access to
servers and storage appliances compliant with the SNIA CIFS Specification
version 1.0 or later.

This package contains DKMS-ready CIFS Linux kernel module with Etersoft extensions.

%prep
%setup -c -T -n %name-%version

%install
mkdir -p %buildroot%_usrsrc/%modname-%version/
cat > %buildroot%_usrsrc/%modname-%version/dkms.conf <<EOF
# DKMS file for Linux CIFS with Etersoft's extensions

PACKAGE_NAME="%modname"
PACKAGE_VERSION="%version"

BUILT_MODULE_NAME[0]="etercifs"
DEST_MODULE_LOCATION[0]="/kernel/fs/cifs/"
REMAKE_INITRD="no"
AUTOINSTALL="YES"
EOF

%post
if [ "$1" == 1 ]
then
  dkms add -m %modname -v %version --rpm_safe_upgrade
fi
%_initdir/%modname build

%preun
if [ "$1" == 0 ]
then
  dkms remove -m %modname -v %version --rpm_safe_upgrade --all
fi

%files
%_usrsrc/%modname-%version/dkms.conf

%changelog
* Fri Mar 12 2010 Vitaly Lipatov <lav@altlinux.ru> 4.5.0-alt1
- new version 4.5.0

* Fri Feb 19 2010 Vitaly Lipatov <lav@altlinux.ru> 4.4.3-alt1
- new version 4.4.5

* Fri Nov 06 2009 Yuri Fil <yurifil@altlinux.org> 4.3.9-alt1
- new version 4.3.9

* Mon Jul 27 2009 Vitaly Lipatov <lav@altlinux.ru> 4.3.8-alt1
- new version 4.3.8

* Thu Jul 09 2009 Vitaly Lipatov <lav@altlinux.ru> 4.3.7-alt1
- new version

* Wed Apr 15 2009 Konstantin Baev <kipruss@altlinux.org> 4.3.6-alt1
- new version
- clear changelog

* Wed Nov 05 2008 Konstantin Baev <kipruss@altlinux.org> 3.7.0-alt1
- rename package, new version

* Thu Jul 31 2008 Boris Savelev <boris@altlinux.org> 1.53-alt1
- new version

* Sat Jan 26 2008 Vitaly Lipatov <lav@altlinux.ru> 1.50c-alt1
- initial build for Korinf project
